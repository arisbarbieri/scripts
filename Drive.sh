#!/bin/sh
if [ $(ls ~/Documents/Zoom/ | wc -l) -lt "2" ]; then
    ./rename.sh
    #/home/aris/Google_Drive_UTN/Aris Barbieri Cavallini/Evidencias III-2021/Clases Lunes noche/
    if [ $(date +%A) = 'lunes' ]; then
        mv ~/Documents/Zoom/* /home/aris/Google_Drive_UTN/Aris\ Barbieri\ Cavallini/Evidencias\ III-2021/Clases\ Lunes\ noche/
    elif [ $(date +%A) = 'martes' ]; then
        mv ~/Documents/Zoom/* /home/aris/Google_Drive_UTN/Aris\ Barbieri\ Cavallini/Evidencias\ III-2021/Clases\ martes\ tarde/
    elif [ $(date +%A) = 'jueves' ]; then
        mv ~/Documents/Zoom/* /home/aris/Google_Drive_UTN/Aris\ Barbieri\ Cavallini/Evidencias\ III-2021/Clases\ jueves\ tarde/
    fi
    #cp ~/Documents/Zoom/* ~/Google_Drive_UTN/Aris\ Barbieri\ Cavallini/
else
    echo "hay más de un archivo en el directorio"
fi
